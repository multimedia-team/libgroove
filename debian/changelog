libgroove (4.3.0-6) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Ondřej Nový <onovy@debian.org>  Sat, 20 Jul 2019 01:04:28 +0200

libgroove (4.3.0-5) unstable; urgency=medium

  * QA upload.
  * Orphan package, set maintainer to qa group

 -- Felipe Sateler <fsateler@debian.org>  Sat, 29 Sep 2018 19:11:10 -0300

libgroove (4.3.0-4) unstable; urgency=medium

  * Team upload.

  [ James Cowgill ]
  * debian/compat: Use debhelper compat 11.
  * debian/control:
    - Drop libgroove-dbg.
    - Bump standards version to 4.1.4.
  * debian/libgroove-dev.docs:
    - Rename from debian/docs to be more explicit.
  * debian/patches:
    - Add patch to fix FTBFS with FFmpeg 4.0. (Closes: #888376)

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field.
  * d/control: Deprecating priority extra as per policy 4.0.1.
  * d/control: Set Vcs-* to salsa.debian.org.
  * d/control: Remove trailing whitespaces.
  * d/rules: Remove trailing whitespaces.

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org.

 -- James Cowgill <jcowgill@debian.org>  Sat, 19 May 2018 14:52:15 +0200

libgroove (4.3.0-3) unstable; urgency=medium

  * Use gbp-pq for patch handling
  * dummyplayer: do not unlock the play head mutex when an underrun occurs
    (Closes: #844137)

 -- Felipe Sateler <fsateler@debian.org>  Tue, 03 Jan 2017 21:30:11 -0300

libgroove (4.3.0-2) unstable; urgency=medium

  * Team upload

  [ Andreas Cadhalpun ]
  * Fix FTBFS by disabling -Werror. (Closes: #810564)

 -- Andreas Cadhalpun <andreas.cadhalpun@googlemail.com>  Mon, 11 Jan 2016 20:28:58 -0300

libgroove (4.3.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version

 -- Felipe Sateler <fsateler@debian.org>  Thu, 29 Oct 2015 21:16:23 -0300

libgroove (4.2.1-1) unstable; urgency=medium

  * New upstream release
    - Fixes build on GNU/Hurd (Closes: #764374)
  * Bump standards version to 3.9.6

 -- Andrew Kelley <superjoe30@gmail.com>  Tue, 07 Oct 2014 17:41:56 +0000

libgroove (4.2.0-1) unstable; urgency=low

  * New upstream release
  * No longer repack orig tarball
  * Simplify copyright file; upstream dropped some third party build deps
  * No longer build-depend on libav-tools
  * Update watch file to canonical format
  * Refresh GNUInstallDirs patch
  * Remove no longer relevant lintan override
  * Update libgrooveplayer long description

 -- Andrew Kelley <superjoe30@gmail.com>  Mon, 29 Sep 2014 00:00:21 +0000

libgroove (4.1.1+dfsg-1) unstable; urgency=low

  * New upstream release

 -- Andrew Kelley <superjoe30@gmail.com>  Fri, 20 Jun 2014 15:04:01 -0700

libgroove (4.1.0+dfsg-1) unstable; urgency=low

  * New upstream release
  * Add groove_playlist_set_fill_mode symbol for libgroove4

 -- Andrew Kelley <superjoe30@gmail.com>  Sun, 15 Jun 2014 16:59:40 -0700

libgroove (4.0.4+dfsg-1) unstable; urgency=low

  * New upstream release
  * Remove patch to link with -lm because it is included in the new release.

 -- Andrew Kelley <superjoe30@gmail.com>  Tue, 03 Jun 2014 10:48:30 -0700

libgroove (4.0.1+dfsg-1) unstable; urgency=low

  * New upstream release
  * Upload to unstable
  * SONAME updated from 3 to 4
  * new binary package: libgroovefingerprinter4
  * new dev package: libgroovefingerprinter-dev
  * debian/control: other -dev packages depend on libgroove-dev
  * debian/copyright: justify the fact that we repack the pristine tar
  * Better package descriptions
  * watch: update dversionmangle so that uscan finds current version
  * add debian/gbp.conf
  * add patch from upstream to link with -lm
  * remove Pre-Depends from debug package

 -- Andrew Kelley <superjoe30@gmail.com>  Wed, 14 May 2014 12:48:42 -0700

libgroove (3.0.6+dfsg-1) experimental; urgency=low

  * Initial release. (Closes: #738461)

 -- Andrew Kelley <superjoe30@gmail.com>  Thu, 20 Feb 2014 13:12:36 -0500
